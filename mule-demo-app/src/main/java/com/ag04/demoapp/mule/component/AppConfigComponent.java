package com.ag04.demoapp.mule.component;

import java.util.ArrayList;
import java.util.List;

import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.ag04.demoapp.model.AppConfig;
import com.ag04.demoapp.service.AppConfigService;
/**
 * 
 * @author dmadunic
 *
 */
@Component
public class AppConfigComponent implements Callable {
	private static final Logger LOG = LoggerFactory.getLogger(AppConfigComponent.class);
	
	@Autowired
	private AppConfigService appConfigService;

	@Override
	public Object onCall(MuleEventContext eventContext) throws Exception {
		MuleMessage message = eventContext.getMessage();
		
		Object payload = message.getPayload();
		LOG.info("Fetching appConfig ----> payload={}", payload);
		if (payload != null  && payload instanceof String) {
			AppConfig result = appConfigService.findByKey((String) payload);
			return result;
		} else {
			List<AppConfig> result = new ArrayList<AppConfig>();
			result = appConfigService.findAll();
			LOG.info("---> result='{}'", result);
			return result;	
		}		
	}
	
}
