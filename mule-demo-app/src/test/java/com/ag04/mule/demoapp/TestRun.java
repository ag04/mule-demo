package com.ag04.mule.demoapp;

import java.util.Properties;

import org.mule.MuleServer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.io.ClassPathResource;
/**
 * 
 * @author dmadunic
 *
 */
public class TestRun {
	private static final Logger LOG = LoggerFactory.getLogger(TestRun.class);
	
    public static void main(String[] args) throws Exception {
        System.setProperty("spring.profiles.active", "local");
        System.setProperty("env-name", "local");
        System.setProperty("mule.home", ".");
        
        ClassPathResource props = new ClassPathResource("mule-app.properties");
        ClassPathResource config = new ClassPathResource("mule-deploy.properties");

        Properties configurationFiles = new Properties();
        configurationFiles.load(config.getInputStream());
        String configs = configurationFiles.getProperty("config.resources");

        MuleServer server = new MuleServer(new String[]{"-props", props.getPath(), "-config", configs});

        server.start(false, true);
        
        LOG.debug("*************************************");
        LOG.debug("*  Demo App is up and running       *");
        LOG.debug("*************************************");

    }
}
