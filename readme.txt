AG04 Mule application template project
======================================

Prerequisites:
----------------
- PostgresSQL with created database and configured user
- HornetQ (2.4.x)
	
- Mule 3.4.0 version installed and MULE_HOME env setup

Configuration:
--------------
1. Copy this project under new name and change the following items in project pom.xml: 
	- artifactId 
	- gorupId 
	- project name
	- project description


2. create database, user and schema in postgres db


3. Open demoapp.properties and edit it to suit your environment and save file under name:
{your-project-name}.local.properties

4. Rename: /mule-demo-app/src/main/resources/META-INF/demoapp/demoapp-context.xml into {your-project-name}-context.xml

5. open this new file and adjust the following line according to the name given in step (2):
<util:properties id="appProperties" location="classpath:/META-INF/demoapp/conf/demoapp.local.properties"/>

7. Open: /mule-demo-app/src/main/app/mule-config.xml
and change queue name "demoapp.audit" to "{your_project_name}.audit":

	<flow name="AuditFlow_jms">
		<jms:inbound-endpoint queue="demoapp.audit" connector-ref="hornetq-connector" />
		<logger message="RECEIVED ---> '#[message.payload]'" level="INFO" />
	</flow>

8. Configure HornetQ
open hornetq-jms.xml in {hornetq_home}/config/non-clustered/
and add following queue at the end of the file:

	<queue name="{your_project_name}.audit">
      <entry name="/queue/{your_project_name}.audit"/>
   </queue

9. Open src/main/app/mule-config.xml
and adjust path to your main configuration file.

<spring:import resource="classpath*:/META-INF/demoapp/demoapp-context.xml"/>


Test it:
====================================
Go to project folder and perform:

mvn clean verify

Open browser and enter the following link:
http://localhost:8080/demoapp/currency/list
