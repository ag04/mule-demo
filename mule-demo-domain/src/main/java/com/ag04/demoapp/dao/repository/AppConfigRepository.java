package com.ag04.demoapp.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.ag04.demoapp.model.AppConfig;

/**
 * 
 * @author dmadunic
 *
 */
public interface AppConfigRepository extends JpaRepository<AppConfig, Long> {

    AppConfig findByKey(String key);

}
