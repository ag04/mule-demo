package com.ag04.demoapp.ws;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

import com.ag04.demoapp.model.AppConfig;
/**
 * 
 * @author dmadunic
 *
 */
@WebService
public interface AppConfigWebService {
	
	@WebMethod
	String findValueForKey(@WebParam(name = "key") String key);
	
	@WebMethod
	AppConfig findByKey(@WebParam(name = "key") String key);
	
	@WebMethod
	List<AppConfig> findAll();
}
