package com.ag04.demoapp.service;

import java.util.List;

import com.ag04.demoapp.model.AppConfig;

public interface AppConfigService {

	String findValueForKey(String key);
	
	AppConfig findByKey(String key);
	
	List<AppConfig> findAll();
}
