package com.ag04.demoapp.service.impl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import com.ag04.demoapp.dao.repository.AppConfigRepository;
import com.ag04.demoapp.model.AppConfig;
import com.ag04.demoapp.service.AppConfigService;
import com.ag04.demoapp.ws.AppConfigWebService;

/**
 * 
 * @author dmadunic
 *
 */
@Service
public class AppConfigServiceImpl implements AppConfigService, AppConfigWebService {
	private static final Logger LOG = LoggerFactory.getLogger(AppConfigServiceImpl.class);
	
	@Autowired
	private AppConfigRepository repository;
	
	@Cacheable(value = "appConfig.findValueForKey")
	public String findValueForKey(String key) {
		LOG.trace("Fetching appConfig for key={}", key);
		AppConfig appConfig = repository.findByKey(key);
		String value = null;
		
		if (appConfig != null) {
			value = appConfig.getValue();
		}
		return value;
	}

	@Override
	@Cacheable(value = "appConfig.findByKey")
	public AppConfig findByKey(String key) {
		LOG.trace("Fetching appConfig for key={}", key);
		AppConfig appConfig = repository.findByKey(key);
		return appConfig;
	}

	@Override
	public List<AppConfig> findAll() {
		return repository.findAll();
	}
}
